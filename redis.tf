resource "aws_elasticache_subnet_group" "elasticache-group" {
  name = var.redis__subnet_group__name
  subnet_ids = var.redis__subnet_group__subnet_ids
}

resource "aws_elasticache_replication_group" "redis-rg" {
  replication_group_description = "Replication group for redis"
  replication_group_id = var.redis__replication_group_id
  automatic_failover_enabled = true
  node_type = var.redis__node_type
  engine = "redis"
  port = 6379
  security_group_ids = [aws_security_group.redis-sgs.id]
  parameter_group_name = "default.redis5.0.cluster.on"
  subnet_group_name = aws_elasticache_subnet_group.elasticache-group.name

  cluster_mode {
    num_node_groups = var.redis__num_node_groups
    replicas_per_node_group = var.redis__replicas_per_node_group
  }

  lifecycle {
    ignore_changes = [number_cache_clusters]
    create_before_destroy = true
  }

  tags = {
    "Cluster Name" = var.redis__tag_cluster_name
    group = var.cache-group-tag
  }

  depends_on = [
    aws_elasticache_subnet_group.elasticache-group,
    aws_security_group.redis-sgs,
  ]
}