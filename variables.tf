variable "cache-group-tag" {type = string}

variable "redis__node_type" {type = string}
variable "redis__subnet_group__subnet_ids" {type = list(string)}
variable "redis__num_node_groups" {type = number}
variable "redis__replicas_per_node_group" {type = number}
variable "redis__tag_cluster_name" {type = string}
variable "redis__replication_group_id" {type = string}
variable "redis__subnet_group__name" {type = string}
variable "redis__security_group_vpc_id" {type = string}
variable "redis__security_group_ingress_security_group_ids" {type = list(string)}