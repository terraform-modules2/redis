output "redis-configuration-endpoint" {
  value = aws_elasticache_replication_group.redis-rg.configuration_endpoint_address
}