resource "aws_security_group" "redis-sgs" {
  vpc_id = var.redis__security_group_vpc_id

  ingress {
    from_port = 6379
    protocol = "TCP"
    to_port = 6379
    security_groups = var.redis__security_group_ingress_security_group_ids
  }
}